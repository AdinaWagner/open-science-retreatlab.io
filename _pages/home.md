---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#594777"
  overlay_image: "/assets/images/night.jpg"
  og_image: "/assets/images/lake_social.jpg"
excerpt: "April 3-7, 2023  <br>
#OpenScienceRetreat23" 
feature_row:
  - image_path: /assets/images/jumping.jpg
    alt: "A woman jumping. Trees surround her."
    title: "A retreat, finally!"
    excerpt: "Have you been waiting for an Open Science event where you can learn, work and reboot?"
    url: "/about/"
    btn_class: "btn--primary"
    btn_label: "About"
  - image_path: /assets/images/april.jpg
    alt: "A notebook with the word April written on it in nice lettering."
    title: "A program that suits your needs."
    excerpt: "Lots of time to think and work, but also to relax and move."
    url: "/program/"
    btn_class: "btn--primary"
    btn_label: "Program"
  - image_path: /assets/images/lake.jpg
    alt: "A lake and mountains."
    title: "A great location"
    excerpt: "With a view of the lake and the alps, this is the perfect venue for a retreat."
    url: "/venue/"
    btn_class: "btn--primary"
    btn_label: "Venue"      
gallerypartners:
  - url: https://www.casus.science
    image_path: /assets/images/supporters/logo-casus-hzdr.jpg
    alt: "Logos CASUS and HZDR"
  - url: https://heidiseibold.com/
    image_path: /assets/images/supporters/logo-heidi.jpg
    alt: "Logo Heidi Seibold"
gallerysponsors:
  - url: https://www.fz-juelich.de/en
    image_path: /assets/images/supporters/logo-fzj.jpg
    alt: "Logo FZJ"
  - url: https://www.osc.uni-muenchen.de
    image_path: /assets/images/supporters/logo-osc.jpg
    alt: "Logo LMU Open Science Center"
  - url: https://os.helmholtz.de
    image_path: /assets/images/supporters/logo-helmholtzos.jpg
    alt: "Logo Helmholtz Open Science"
gallerysupporters:
  - url: https://open-science-freelancers.gitlab.io/
    image_path: /assets/images/supporters/logo-osfreelancers.png
    alt: "Logo Open Science Freelancers"
  - url: https://skillsweaving.com
    image_path: /assets/images/supporters/logo-skillsweaving.png
    alt: "Logo Skills Weaving"
  - url: https://www.trovbase.com
    image_path: /assets/images/supporters/logo-trovbase.png
    alt: "Logo Trovbase"
  - url: https://access2perspectives.org/
    image_path: /assets/images/supporters/logo-a2p.png
    alt: "Logo Access2Perspectives"
  - url: https://libscie.org/
    image_path: /assets/images/supporters/logo-libscie.png
    alt: "Logo Lieberate Science"
---

Whether you are just getting started with Open Science or are already an open hero.  
Whether you are a researcher or provide research services.  
**If you are ready for a week of new ideas, productivity and rebooting: this retreat is for you!**
{: .text-center}

{% include feature_row %}


## Co-organisers
The event is co-organised by the [Scultetus
Center](https://www.casus.science/scultetus/) and Heidi Seibold. The Scultetus
Center, a forthcoming international meeting place of science, belongs to the
Görlitz/Germany-based Center for Advanced Systems Understanding (CASUS) at
Helmholtz-Zentrum Dresden-Rossendorf (HZDR).

{% include gallery id="gallerypartners" layout="half" %}


## Sponsors
The following institutions support the event financially / through travel stipends: 

{% include gallery id="gallerysponsors" layout="third" %}


## Supporters
The following institutions support the event by helping in organising:

{% include gallery id="gallerysupporters" layout="third" %}


