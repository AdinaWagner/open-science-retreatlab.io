---
permalink: /program/
layout: splash
header:
  overlay_color: "#594777"
author_profile: false
title: "Program"
---

To view the schedule please push the button:
[View schedule](/schedule){: .btn .btn--primary .btn--large} 
<!-- [Schedule](https://docs.google.com/spreadsheets/d/1_eDORwvv-cahAtrO6aAqgeMH2MV6wiTzxON8olxxkkQ/edit?usp=sharing){: .btn .btn--primary .btn--large} -->

This event is 
- part [unconference](/program/#unconference), 
- part [focused work](/program/#time-to-focus), 
- part [rebooting](/program/#time-to-reboot).

We also offer some neat [specials](/program/#specials) to boost your 
career.

![A notebook with the word April written on it](/assets/images/april.jpg){: width="300" .align-center}


## Unconference

As a participant you can shape the unconference program (mornings). How? We
will use Mattermost boards.  You can formulate your ideas and discuss the
ideas of others. On the first day of the event we will decide on which ideas we
want to pursue.

[Shape the program](https://chat.science-retreat.org/boards/workspace/qtjgpyyqk3bn3kjwgabh6t8h9r/bdutzkeatcigs9bh3m36iqmkrew/v8puyqmijfbyyjd3prpx8cxe4hc){: .btn .btn--primary}
(only accessbile for participants)

The idea for shaping the program this way comes from the [ROpenSci unconferences](https://unconf16.ropensci.org/#schedule). 

As it is an unconferece, the participants decide on the topics we will cover, but here are some topics that we will likely touch upon:

   - Open research software
   - FAIR principles for data and code
   - Open access and scientific publishing
   - Preregistration and registered reports
   - Intellectual property versus Open Science
   - Incentives for Open Science and good scientific practice
   - Social change and change management in research
   - Reproducible research 
   - Teaching and learning Open Science practices
   - Open Science support 
   - …


## Time to focus

The afternoons are your time. You can focus on your own work, work with others
on things we did in the morning, or take time to ponder/plan (if you want, with
support/coaching).


## Time to reboot

To get you out of your work routine, we have a bunch of activities planned for you.

- Walks
- Jogging
- Yoga
- Reflection time
- Social activities


## Specials

### Career coaching
We offer free 1-on-1 career coaching sessions to get you thinking about where
you want to go next.

At times, it can be challenging to achieve your professional
ambitions. In such cases, an outsider perspective can offer
more clarity on your personal situation. The coaching
facilitates personal reflection and a pro-active approach
towards achieving your goals. Ideally, this allows you to
identify actionable next steps and move ahead.

During the 1-1 coaching sessions (+/- 40 minutes), we can
explore how to overcome general and specific challenges
associated with career development in and beyond
academia. Particularly, we can discuss:

- Designing a personal career development plan
- Professional skills development
- Preparing for career transitions
- Self-employment or entrepreneurship
- Fundraising strategies
- Other aspects related to career development


### Workshop: *Open Science: The Strategist’s Approach*

Being a researcher involves much more than collecting
data, performing analyses, and publishing outcomes. It
requires to stay on top of the latest developments, not only
in your field of research, but also on topics related to the
wider research environment.

In this mini-workshop, we will explore how Open Science
affects (early-career) researchers, both in the context of
research and career development. Specifically, we will
illustrate how to mobilize your Strategist skills to achieve
professional ambitions and maximize your impact. In the
context of the Open Science Retreat, we will focus on the
broader context of open science, the diversity of roles in
this field, and how you can position yourself in line with
your career goals.

Skills Weaving advocates a pro-active approach towards
professional development in the context of career
advancement for academics. We share resources and tools
that allow you to achieve your career ambitions.

#### About the trainer: Wino Wijnen, PhD

Wino has over 15 years of experience in academic
research and innovation management. Trained as a
molecular biologist, he obtained his PhD at the University
of Amsterdam (NL) before continuing as a Postdoctoral
Researcher and eventually co-groupleader at the
University of Zurich (CH).

Since, he has been working as an innovation manager
and career consultant in Zurich, mainly focusing on
fundraising, project design, and career development
strategies for individual scientists and research consortia.


