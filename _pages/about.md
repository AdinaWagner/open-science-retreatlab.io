---
permalink: /about/
layout: splash
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/lake.jpg
author_profile: false
title: "About"
---

The Open Science Retreat is an event from Open Science enthusiast for Open Science enthusiasts.
The main goals are:

- Learning
- Getting stuff done
- Networking
- Reflecting
- Rebooting

---

The event is co-organised by the [Scultetus
Center](https://www.casus.science/scultetus/) and [Heidi
Seibold](https://heidiseibold.com/).  The Scultetus Center, a forthcoming
international meeting place of science, belongs to the Görlitz/Germany-based
[Center for Advanced Systems Understanding](https://www.casus.science/) (CASUS)
at Helmholtz-Zentrum Dresden-Rossendorf (HZDR).

